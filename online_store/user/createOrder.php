<?php
include("../function.php");
$header = getallheaders();
$data = json_decode(file_get_contents('php://input'));
if(isset($header['Authorization'] )){
    if($data != []){  
        $auth = $header['Authorization'];
        if (avail($auth) && substr($auth, 0, 7) === 'Bearer ') {
            if(isset($data->order)){
                $con = connectDB();
                $auth = substr($auth, 7);
                $user_id = checkToken($con, $auth);
                if ($user_id) {
                    $order_product = [];
                    $price_all_total = 0;
                    $quantity_maximum = 0;
                    foreach ($data->order as $key) {
                        $product_id = $key->product_id; 
                        $quantity = $key->quantity;
                        $diskon_id = $key->diskon_id;
                        $product_data = "SELECT p.product_id,p.product_name,p.quantity as quantity_maximum,p.price as price_per_unit,o.value as total_diskon_price,o.name as system_diskon FROM product p join product_diskon pd on p.product_id = pd.product_id join option o on o.option_id = pd.option_id where p.product_id = '$product_id' and p.quantity > '$quantity' and pd.diskon_id = '$diskon_id' and now() between start_date and end_date";
                        $product_data = $con->query($product_data);
                        if($product_data && $product_data->num_rows > 0){
                            $product_data = JsonParser($product_data)[0];
                            $product_name = $product_data['product_name'];
                            $quantity_maximum = $product_data['quantity_maximum'];
                            $total_price_not_diskon = ((int)$product_data['price_per_unit'] *(int)$quantity);
                            if($product_data['system_diskon'] == 'percentage'){
                                $total_price = $total_price_not_diskon*((100-(int)$product_data['total_diskon_price'])/100);
                                $price_all_total = $price_all_total+$total_price;
                            }else{
                                $total_price = (int)$total_price_not_diskon-(int)$product_data['total_diskon_price'];
                                $price_all_total = $price_all_total+$total_price;
                            }
                            $product_order = ['product_id'=>$product_id,'diskon_id'=>$diskon_id,'price_total'=>$total_price,'total_price_not_diskon'=>$total_price_not_diskon,'quantity'=>$quantity,'quantity_maximum'=>$quantity_maximum,'product_name'=>$product_name];
                            array_push($order_product,$product_order);   
                        }else{
                            array_push($order_product,null); 
                        }
                    }

                    if($order_product != []){
                        $sql = "INSERT INTO `order`(user_id, order_status_id,price_total) VALUES ('$user_id',1,'$price_all_total');";
                        $sqlCek = $con->query($sql); 
                        if($sqlCek){
                            $order_id = $con->insert_id;
                            foreach ($order_product as $product_for_order) {
                                if($product_for_order == null){
                                    header('HTTP/1.1 400 Bad Request');
                                    $result = array(
                                        "response" => "Error",
                                        "message" => "Salah Satu Product Sudah Tidak Dapat Dipesan"
                                    );

                                    $sqlDeleteOrder = "DELETE FROM `order` WHERE order_id ='$order_id'";
                                    $sql_delete_order = $con->query($sqlDeleteOrder);

                                    $sqlDelete = "DELETE FROM order_product WHERE order_id ='$order_id'";
                                    $sql_delete = $con->query($sqlDelete);

                                    $sqlUpdate = "UPDATE product SET quantity='$quantity_maximum' WHERE order_id = '$order_id'";
                                    $sqlCekUpdate = $con->query($sqlUpdate);

                                    writeAPI(["api" => "createOrder", "auth" => $auth, "user_id" => isset($user_id) ? $user_id : null, "data" => $data]);
                                    if (isset($result))
                                        die(json_encode($result));
                                }else{
                                    $product_name = $product_for_order['product_name'];
                                    $product_id = $product_for_order['product_id'];
                                    $quantity = $product_for_order['quantity'];
                                    $price_total_not_diskon = $product_for_order['total_price_not_diskon'];
                                    $diskon_id = $product_for_order['diskon_id'];
                                    $quantity_maximum = $product_for_order['quantity_maximum'];
                                    $quantity_sisa = (int)$quantity_maximum-(int)$quantity;
                                    $sqlOrder = "INSERT INTO order_product(order_id,product_id,product_name,quantity,price,diskon_id) VALUES ('$order_id','$product_id','$product_name','$quantity','$price_total_not_diskon','$diskon_id');";  
                                    $sql_product = $con->query($sqlOrder);
                                    if($sql_product){
                                        if($quantity_sisa > 0){
                                            $sqlUpdate = "UPDATE product SET quantity='$quantity_sisa' WHERE product_id = '$product_id'";
                                            $sqlCekUpdate = $con->query($sqlUpdate);
                                            header('HTTP/1.1 200 OK');
                                            $result = array(
                                                "response" => "Success",
                                                "message" => "Data Berhasil Ditambahkan"
                                            );
                                            writeAPI(["api" => "createOrder", "auth" => $auth, "user_id" => isset($user_id) ? $user_id : null, "data" => $data]);
                                            if (isset($result))
                                                die(json_encode($result));
                                        }else{
                                            header('HTTP/1.1 400 Bad Request');
                                            $result = array(
                                            "response" => "Error",
                                            "message" => "Quantity yang Dipesan Melebihi Quantity yang tersedia"
                                            );
                                        
                                        $sqlDeleteOrder = "DELETE FROM `order` WHERE order_id ='$order_id'";
                                        $sql_delete_order = $con->query($sqlDeleteOrder);

                                        $sqlDelete = "DELETE FROM order_product WHERE order_id ='$order_id'";
                                        $sql_delete = $con->query($sqlDelete);

                                        $sqlUpdate = "UPDATE product SET quantity='$quantity_maximum' WHERE product_id = '$product_id'";
                                        $sqlCekUpdate = $con->query($sqlUpdate);
                                        
                                        writeAPI(["api" => "createOrder", "auth" => $auth, "user_id" => isset($user_id) ? $user_id : null, "data" => $data]);
                                        if (isset($result))
                                            die(json_encode($result));

                                        }
                                    }else{
                                        header('HTTP/1.1 500 Internal Server Error');
                                            $result = array(
                                            "response" => "Error",
                                                "data" => array(
                                                    "error" => base64_encode($sql_product)
                                                )
                                            );
                                        }
                                        
                                        $sqlDeleteOrder = "DELETE FROM `order` WHERE order_id ='$order_id'";
                                        $sql_delete_order = $con->query($sqlDeleteOrder);

                                        $sqlDelete = "DELETE FROM order_product WHERE order_id ='$order_id'";
                                        $sql_delete = $con->query($sqlDelete);

                                        $sqlUpdate = "UPDATE product SET quantity='$quantity_maximum' WHERE product_id = '$product_id'";
                                        $sqlCekUpdate = $con->query($sqlUpdate);
                                        
                                        writeAPI(["api" => "createOrder", "auth" => $auth, "user_id" => isset($user_id) ? $user_id : null, "data" => $data]);
                                        if (isset($result))
                                            die(json_encode($result));
                                }
                            }
                        }else{
                            header('HTTP/1.1 500 Internal Server Error');
                            $result = array(
                            "response" => "Error",
                                "data" => array(
                                    "error" => base64_encode($sqlCek)
                                )
                            );
                        } 
                    }else{
                        header('HTTP/1.1 400 Bad Request');
                            $result = array(
                                "response" => "Error",
                                "message" => "Pastikan Parameter yang dikirim telah dilengkapi semua"
                            );
                    }
                } else {
                    header('HTTP/1.1 401 Unauthorized');
                    $result = array(
                        "response" => "Error",
                        "message" => "Unauthorized"
                    );
                }
            }else{
                header('HTTP/1.1 400 Bad Request');
                    $result = array(
                        "response" => "Error",
                        "message" => "Pastikan Parameter yang dikirim telah dilengkapi semua"
                    );
            }
        }else{
            header('HTTP/1.1 401 Unauthorized');
            $result = array(
                "response" => "Error",
                "message" => "Unauthorized"
            );
        }
        writeAPI(["api" => "createOrder", "auth" => $auth, "user_id" => isset($user_id) ? $user_id : null, "data" => $data]);
    }else{
        header('HTTP/1.1 400 Bad Request');
        $result = array(
                "response" => "Error",
                "message" => "Pastikan Method dan Parameter yang Dikirim Telah Sesuai"
            );
        writeAPI(["api" => "createOrder", "data" => $data]);
    }
    if (isset($result))
        die(json_encode($result));
}else{
    header('HTTP/1.1 404 Not Found');
    writeAPI(["api" => "createOrder", "data" => $data]);
}