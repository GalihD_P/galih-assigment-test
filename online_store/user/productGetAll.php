<?php
include("../function.php");
$header = getallheaders();
$data = array_merge($_GET,$_POST);
if(isset($header['Authorization'])){
    $auth = $header['Authorization'];
    if (avail($auth) && substr($auth, 0, 7) === 'Bearer ') {
        $con = connectDB();
        $auth = substr($auth, 7);
        $user_id = checkToken($con, $auth);
        if ($user_id) {
            $sql = "SELECT p.*,count(product_name) as total_diskon FROM product p join product_diskon pd on p.product_id = pd.product_id join option o on o.option_id = pd.option_id where now() between start_date and end_date GROUP BY product_name";
            $sqlCek = $con->query($sql);
            if($sqlCek){
                $sqlCek = JsonParser($sqlCek);
                $sqlAll = "SELECT * FROM product";
                $sqlAllCek = $con->query($sqlAll);
                $data_push = [];
                foreach ($sqlAllCek as $productAll) {
                    if($productAll['quantity'] != 0){
                        foreach ($sqlCek as $productDiskon) {
                            if($productAll['product_id'] != $productDiskon['product_id']){
                                $productAll['total_diskon'] = 0;
                                array_push($data_push, $productAll);
                            }else{
                                array_push($data_push, $productDiskon);
                            }
                        }
                    }
                }

                header('HTTP/1.1 200 OK');
                $result = array(
                    "response" => "Success",
                    "data" => $data_push
                );

            }else{
                header('HTTP/1.1 500 Internal Server Error');
                $result = array(
                    "response" => "Error",
                    "data" => array(
                        "error" => base64_encode($sqlCek)
                    )
                );
            }
        } else {
            header('HTTP/1.1 401 Unauthorized');
            $result = array(
                "response" => "Error",
                "message" => "Unauthorized"
            );
        }
    }else{
        header('HTTP/1.1 401 Unauthorized');
        $result = array(
            "response" => "Error",
            "message" => "Unauthorized"
        );
    }
    writeAPI(["api" => "productGetAll", "auth" => $auth, "user_id" => isset($user_id) ? $user_id : null, "data" => $data]);
    if (isset($result))
        die(json_encode($result));
}else{
    header('HTTP/1.1 404 Not Found');
    writeAPI(["api" => "productGetAll", "data" => $data]);
}