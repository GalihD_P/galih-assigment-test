<?php
include("../function.php");
$header = getallheaders();
$data = array_merge($_GET,$_POST);
if(isset($header['Authorization'])){
    $auth = $header['Authorization'];
    if (avail($auth) && substr($auth, 0, 7) === 'Bearer ') {
        $con = connectDB();
        $auth = substr($auth, 7);
        $user_id = checkToken($con, $auth);
        if($user_id){
            if(isset($data['product_id'])){
                $product_id = $data['product_id'];
                $sql = "SELECT * FROM product p join product_diskon pd on p.product_id = pd.product_id join option o on o.option_id = pd.option_id where p.product_id ='$product_id' and now() between start_date and end_date";
                $sqlCek = $con->query($sql);

                if($sqlCek){
                    $sqlCek = JsonParser($sqlCek);
                    header('HTTP/1.1 200 OK');
                    $result = array(
                        "response" => "Success",
                        "data" => $sqlCek
                    );
                }else{
                    header('HTTP/1.1 500 Internal Server Error');
                    $result = array(
                    "response" => "Error",
                        "data" => array(
                            "error" => base64_encode($sqlCek)
                        )
                    );
                }
            }else{
                header('HTTP/1.1 400 Bad Request');
                $result = array(
                    "response" => "Error",
                    "message" => "Pastikan Parameter yang dikirim telah dilengkapi semua"
                );
            }
        }else {
            header('HTTP/1.1 401 Unauthorized');
            $result = array(
                "response" => "Error",
                "message" => "Unauthorized"
            );
        }
    }else{
        header('HTTP/1.1 401 Unauthorized');
        $result = array(
            "response" => "Error",
            "message" => "Unauthorized"
        );
    }
    writeAPI(["api" => "productDetailDiskon", "auth" => $auth, "user_id" => isset($user_id) ? $user_id : null, "data" => $data]);
    if (isset($result))
        die(json_encode($result));
}else{
    header('HTTP/1.1 404 Not Found');
    writeAPI(["api" => "productDetailDiskon", "data" => $data]);
}