<?php
include("../function.php");
$header = getallheaders();
$data = array_merge($_GET,$_POST);
if(isset($header['Authorization'])){
    $auth = $header['Authorization'];
    if (avail($auth) && substr($auth, 0, 7) === 'Bearer ') {
        $con = connectDB();
        $auth = substr($auth, 7);
        $admin_id = checkTokenAdmin($con, $auth);
        if ($admin_id) {
            $sql = "SELECT * FROM product;";
            $sqlCek = $con->query($sql);
            if($sqlCek){
                $sqlCek = JsonParser($sqlCek);
                header('HTTP/1.1 200 OK');
                $result = array(
                    "response" => "Success",
                    "data" => $sqlCek
                );
            }else{
                $sqlCek = JsonParser($sqlCek);
                header('HTTP/1.1 500 Internal Server Error');
                $result = array(
                    "response" => "Error",
                    "data" => array(
                        "error" => base64_encode($sqlCek)
                    )
                );
            }
        } else {
            header('HTTP/1.1 401 Unauthorized');
            $result = array(
                "response" => "Error",
                "message" => "Unauthorized"
            );
        }
    }else{
        header('HTTP/1.1 401 Unauthorized');
        $result = array(
            "response" => "Error",
            "message" => "Unauthorized"
        );
    }
    writeAPI(["api" => "productAllGet", "auth" => $auth, "player_id" => isset($admin_id) ? $admin_id : null, "data" => $data]);
    if (isset($result))
        die(json_encode($result));
}else{
    header('HTTP/1.1 404 Not Found');
    writeAPI(["api" => "productAllGet", "data" => $data]);
}