<?php
include("../function.php");
$header = getallheaders();
$data = array_merge($_GET,$_POST);
if(isset($header['Authorization'])){
    if($data != []){  
        $auth = $header['Authorization'];
        if (avail($auth) && substr($auth, 0, 7) === 'Bearer ') {
            if(isset($data['product_id'])){
                $product_id = $data['product_id'];
                $con = connectDB();
                $auth = substr($auth, 7);
                $admin_id = checkTokenAdmin($con, $auth);
                if ($admin_id) {
                    $sql = "SELECT * FROM product WHERE product_id='$product_id';";
                    $sqlCek = $con->query($sql);
                    if($sqlCek){
                        $sqlCek = JsonParser($sqlCek);
                        header('HTTP/1.1 200 OK');
                        $result = array(
                            "response" => "Success",
                            "data" => $sqlCek
                        );

                    }else{
                        header('HTTP/1.1 500 Internal Server Error');
                        $result = array(
                            "response" => "Error",
                            "data" => array(
                                "error" => base64_encode($sqlCek)
                            )
                        );
                    }
                } else {
                    header('HTTP/1.1 401 Unauthorized');
                    $result = array(
                        "response" => "Error",
                        "message" => "Unauthorized"
                    );
                }
            }else{
                header('HTTP/1.1 400 Bad Request');
                    $result = array(
                        "response" => "Error",
                        "message" => "Pastikan Parameter yang dikirim telah dilengkapi semua"
                    );
                }
        }else{
            header('HTTP/1.1 401 Unauthorized');
            $result = array(
                "response" => "Error",
                "message" => "Unauthorized"
            );
        }
        writeAPI(["api" => "productDetail", "auth" => $auth, "player_id" => isset($admin_id) ? $admin_id : null, "data" => $data]);
    }else{
        header('HTTP/1.1 400 Bad Request');
        $result = array(
                "response" => "Error",
                "message" => "Pastikan Method dan Parameter yang Dikirim Telah Sesuai"
            );
        writeAPI(["api" => "productDetail", "data" => $data]);
    }
    if (isset($result))
        die(json_encode($result));
}else{
    header('HTTP/1.1 404 Not Found');
    writeAPI(["api" => "productDetail", "data" => $data]);
}