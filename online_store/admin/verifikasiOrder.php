<?php
include("../function.php");
$header = getallheaders();
$data = array_merge($_GET,$_POST);
if(isset($header['Authorization'])){
    if($data != []){  
        $auth = $header['Authorization'];
        if (avail($auth) && substr($auth, 0, 7) === 'Bearer ') {
            if(isset($data['order_status']) && isset($data['order_id'])){
                $con = connectDB();
                $auth = substr($auth, 7);
                $admin_id = checkTokenAdmin($con, $auth);
                $order_status = $data['order_status'];
                $order_id = $data['order_id'];
                if ($admin_id) {
                    $sql = "UPDATE `order` SET order_status_id=$order_status WHERE order_id = '$order_id'";
                    $sqlCek = $con->query($sql);
                    if($sqlCek){
                        header('HTTP/1.1 200 OK');
                        $result = array(
                            "response" => "Success",
                            "message" => "Order Status Berhasil Diverifikasi"
                        );

                    }else{
                        header('HTTP/1.1 500 Internal Server Error');
                        $result = array(
                            "response" => "Error",
                            "data" => array(
                                "error" => base64_encode($sqlCek)
                            )
                        );
                    }
                } else {
                    header('HTTP/1.1 401 Unauthorized');
                    $result = array(
                        "response" => "Error",
                        "message" => "Unauthorized"
                    );
                }
            }else{
                header('HTTP/1.1 400 Bad Request');
                    $result = array(
                        "response" => "Error",
                        "message" => "Pastikan Parameter yang dikirim telah dilengkapi semua"
                    );
                }
        }else{
            header('HTTP/1.1 401 Unauthorized');
            $result = array(
                "response" => "Error",
                "message" => "Unauthorized"
            );
        }
        writeAPI(["api" => "verifikasiOrder", "auth" => $auth, "admin_id" => isset($admin_id) ? $admin_id : null, "data" => $data]);
    }else{
        header('HTTP/1.1 400 Bad Request');
        $result = array(
                "response" => "Error",
                "message" => "Pastikan Method dan Parameter yang Dikirim Telah Sesuai"
            );
        writeAPI(["api" => "verifikasiOrder", "data" => $data]);
    }
    if (isset($result))
        die(json_encode($result));
}else{
    header('HTTP/1.1 404 Not Found');
    writeAPI(["api" => "verifikasiOrder", "data" => $data]);
}