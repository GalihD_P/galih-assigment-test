<?php
include("../function.php");
$header = getallheaders();
$data = array_merge($_GET,$_POST);
if(isset($header['Authorization'] )){
    if($data != []){  
        $auth = $header['Authorization'];
        if (avail($auth) && substr($auth, 0, 7) === 'Bearer ') {
            if(isset($data['diskon_name']) && isset($data['product_id']) && isset($data['start_date']) && isset($data['end_date']) && isset($data['option_id'])){
                $con = connectDB();
                $auth = substr($auth, 7);
                $admin_id = checkTokenAdmin($con, $auth);
                if ($admin_id) {
                    $diskon_name = $data['diskon_name'];
                    $start_date = $data['start_date'];
                    $end_date = $data['end_date'];
                    $option_id = $data['option_id'];
                    $product_id = $data['product_id'];

                    $sql = "INSERT INTO product_diskon(diskon_name,start_date,end_date,option_id,product_id,admin_id) VALUES ('$diskon_name', '$start_date','$end_date','$option_id','$product_id','$admin_id');";
                    
                    $sqlCek = $con->query($sql);
                    if($sqlCek){
                        header('HTTP/1.1 201 Created');
                        $result = array(
                            "response" => "Success",
                            "message" => "Product Diskon Berhasil DiSet"
                        );
                    }else{
                        header('HTTP/1.1 500 Internal Server Error');
                        $result = array(
                            "response" => "Error",
                            "data" => array(
                                "error" => base64_encode($sqlCek)
                            )
                        );
                    }
                } else {
                    header('HTTP/1.1 401 Unauthorized');
                    $result = array(
                        "response" => "Error",
                        "message" => "Unauthorized"
                    );
                }
            }else{
                header('HTTP/1.1 400 Bad Request');
                    $result = array(
                        "response" => "Error",
                        "message" => "Pastikan Parameter yang dikirim telah dilengkapi semua"
                    );
                }
        }else{
            header('HTTP/1.1 401 Unauthorized');
            $result = array(
                "response" => "Error",
                "message" => "Unauthorized"
            );
        }
        writeAPI(["api" => "diskonSetProduct", "auth" => $auth, "user_id" => isset($admin_id) ? $admin_id : null, "data" => $data]);
    }else{
        header('HTTP/1.1 400 Bad Request');
        $result = array(
                "response" => "Error",
                "message" => "Pastikan Method dan Parameter yang Dikirim Telah Sesuai"
            );
        writeAPI(["api" => "diskonSetProduct", "data" => $data]);
    }
    if (isset($result))
        die(json_encode($result));
}else{
    header('HTTP/1.1 404 Not Found');
    writeAPI(["api" => "diskonSetProduct", "data" => $data]);
}