<?php
include("../function.php");
$header = getallheaders();
$data = array_merge($_GET,$_POST);
if(isset($header['Authorization'] )){
    if($data != []){  
        $auth = $header['Authorization'];
        if (avail($auth) && substr($auth, 0, 7) === 'Bearer ') {
            if(isset($data['product_name']) && isset($data['price']) && isset($data['quantity']) && isset($data['product_description'])){
                $con = connectDB();
                $auth = substr($auth, 7);
                $admin_id = checkTokenAdmin($con, $auth);
                if ($admin_id) {

                    $product_name = mysqli_real_escape_string($con, preg_replace("/[^A-Za-z0-9]/", ' ', strtoupper($data['product_name'])));
                    $price = mysqli_real_escape_string($con, preg_replace("/[^A-Za-z0-9]/", '', strtoupper($data['price'])));
                    $quantity = mysqli_real_escape_string($con, preg_replace("/[^A-Za-z0-9]/", '', strtoupper($data['quantity'])));
                    $product_description = mysqli_real_escape_string($con, preg_replace("/[^A-Za-z0-9]/", '', strtoupper($data['product_description'])));

                    $sql = "INSERT INTO product(product_name, price,quantity,product_description,admin_id) VALUES ('$product_name', '$price','$quantity','$product_description','$admin_id');";
                    
                    $sqlCek = $con->query($sql);
                    if($sqlCek){
                        header('HTTP/1.1 201 Created');
                        $result = array(
                            "response" => "Success",
                            "message" => "Product Berhasil Ditambahkan"
                        );
                    }else{
                        header('HTTP/1.1 500 Internal Server Error');
                        $result = array(
                            "response" => "Error",
                            "data" => array(
                                "error" => base64_encode($sqlCek)
                            )
                        );
                    }
                } else {
                    header('HTTP/1.1 401 Unauthorized');
                    $result = array(
                        "response" => "Error",
                        "message" => "Unauthorized"
                    );
                }
            }else{
                header('HTTP/1.1 400 Bad Request');
                    $result = array(
                        "response" => "Error",
                        "message" => "Pastikan Parameter yang dikirim telah dilengkapi semua"
                    );
                }
        }else{
            header('HTTP/1.1 401 Unauthorized');
            $result = array(
                "response" => "Error",
                "message" => "Unauthorized"
            );
        }
        writeAPI(["api" => "productAdd", "auth" => $auth, "user_id" => isset($admin_id) ? $admin_id : null, "data" => $data]);
    }else{
        header('HTTP/1.1 400 Bad Request');
        $result = array(
                "response" => "Error",
                "message" => "Pastikan Method dan Parameter yang Dikirim Telah Sesuai"
            );
        writeAPI(["api" => "productAdd", "data" => $data]);
    }
    if (isset($result))
        die(json_encode($result));
}else{
    header('HTTP/1.1 404 Not Found');
    writeAPI(["api" => "productAdd", "data" => $data]);
}