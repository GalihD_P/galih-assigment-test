import random

CekX = random.randint(0,5);
CekY = random.randint(0,3);

characterPosition = "| X |"

characterX = 0
characterY = 3
characterXmax = 5
characterXmin = 0
characterYmax = 3
characterYmin = 0

board = [["|   |" for a in range(6)] for b in range(4)]

board[characterY][characterX] = characterPosition

board[1][1] = "| # |"
board[1][2] = "| # |"
board[1][3] = "| # |"
board[2][3] = "| # |"
board[2][5] = "| # |"
board[3][1] = "| # |"


def random_treasure():
    CekX = random.randint(0,5)
    CekY = random.randint(0,3)
    if board[CekY][CekX] != "| # |" and board[CekY][CekX] != "| X |"  :
        board[CekY][CekX] = "| $ |"
    else :
        CekX = random.randint(0,5)
        CekY = random.randint(0,3)
        if board[CekY][CekX] != "| # |" and board[CekY][CekX] != "| X |"  :
            board[CekY][CekX] = "| $ |"
        

random_treasure()

print("Welcome in Treasure Hunt Game")

turns = 0
while turns < 2 :
    for i in board:
        print(" ---- ----- ----- ----- ----- -----")
        print(" ".join(i))
        print(" ----- ----- ----- ----- ----- -----")

    print("Instructions :")
    print("Up : W")
    print("Down :S")
    print("Rigth : D")
    print("Left : A")

    direction =  input("Please enter one of  the above options: ")
    if direction == "W" or direction == "w":
        if characterY-1 >= characterYmin :
            if board[characterY-1][characterX] == "| $ |" :
                board[characterY][characterX] = "|   |"
                characterY -= 1
                board[characterY][characterX] = "| X |"
                random_treasure()
            elif board[characterY-1][characterX] != "| # |" :
                board[characterY][characterX] = "|   |"
                characterY -= 1
                board[characterY][characterX] = "| X |"
    elif direction == "S" or direction == "s":
        if characterY+1 <= characterYmax :
            if board[characterY+1][characterX] == "| $ |" :
                board[characterY][characterX] = "|   |"
                characterY += 1
                board[characterY][characterX] = "| X |"
                random_treasure()
            elif board[characterY+1][characterX] != "| # |" :
                board[characterY][characterX] = "|   |"
                characterY += 1
                board[characterY][characterX] = "| X |"        
    elif direction == "A" or direction == "a":
        if characterX-1 >= characterXmin :
            if board[characterY][characterX-1] == "| $ |" :
                board[characterY][characterX] = "|   |"
                characterX -= 1
                board[characterY][characterX] = "| X |"
                random_treasure()
            elif board[characterY][characterX-1] != "| # |" :
                board[characterY][characterX] = "|   |"
                characterX -= 1
                board[characterY][characterX] = "| X |"
    elif direction == "D" or direction == "d":
        if characterX+1 <= characterXmax :
            if board[characterY][characterX+1] == "| $ |" :
                board[characterY][characterX] = "|   |"
                characterX += 1
                board[characterY][characterX] = "| X |"
                random_treasure()
            elif board[characterY][characterX+1] != "| # |" :
                board[characterY][characterX] = "|   |"
                characterX += 1
                board[characterY][characterX] = "| X |"
    else :
        data = input("Apakah anda akan selesai dengan game ini (Y/N) :")
        if data == "Y" or data == "y" : 
            break
                
    

