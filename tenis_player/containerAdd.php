<?php
include("function.php");
$header = getallheaders();
$data = array_merge($_GET,$_POST);
if(isset($header['Authorization'] )){
    if($data != []){  
        $auth = $header['Authorization'];
        if (avail($auth) && substr($auth, 0, 7) === 'Bearer ') {
            if(isset($data['container_name']) && isset($data['capacity'])){
                $con = connectDB();
                $auth = substr($auth, 7);
                $player_id = checkToken($con, $auth);
                if ($player_id) {
                    $container_name = mysqli_real_escape_string($con, preg_replace("/[^A-Za-z0-9]/", ' ', strtoupper($data['container_name'])));
                    $capacity = mysqli_real_escape_string($con, preg_replace("/[^A-Za-z0-9]/", '', strtoupper($data['capacity'])));
                    $sql = "INSERT INTO container(container_name, capacity) VALUES ('$container_name', '$capacity');";
                    $sqlCek = $con->query($sql);
                    if($sqlCek){
                        $container_id_last = $con->insert_id;
                        $sql2 = "INSERT INTO player_to_container(player_id, container_id) VALUES ('$player_id', '$container_id_last');";
                        $sqlCek2 = $con->query($sql2);
                        if($sqlCek2){
                            header('HTTP/1.1 201 Created');
                            $result = array(
                                "response" => "Success",
                                "message" => "Container Berhasil Ditambahkan"
                            );
                        }else{
                            header('HTTP/1.1 500 Internal Server Error');
                            $result = array(
                                "response" => "Error",
                                "data" => array(
                                    "error" => base64_encode($sqlCek2)
                                )
                            );
                        }
                    }else{
                        header('HTTP/1.1 500 Internal Server Error');
                        $result = array(
                            "response" => "Error",
                            "data" => array(
                                "error" => base64_encode($sqlCek)
                            )
                        );
                    }
                } else {
                    header('HTTP/1.1 401 Unauthorized');
                    $result = array(
                        "response" => "Error",
                        "message" => "Unauthorized"
                    );
                }
            }else{
                header('HTTP/1.1 400 Bad Request');
                    $result = array(
                        "response" => "Error",
                        "message" => "Pastikan Parameter yang dikirim telah dilengkapi semua"
                    );
                }
        }else{
            header('HTTP/1.1 401 Unauthorized');
            $result = array(
                "response" => "Error",
                "message" => "Unauthorized"
            );
        }
        writeAPI(["api" => "containerAdd", "auth" => $auth, "player_id" => isset($player_id) ? $player_id : null, "data" => $data]);
    }else{
        header('HTTP/1.1 400 Bad Request');
        $result = array(
                "response" => "Error",
                "message" => "Pastikan Method dan Parameter yang Dikirim Telah Sesuai"
            );
        writeAPI(["api" => "containerAdd", "data" => $data]);
    }
    if (isset($result))
        die(json_encode($result));
}else{
    header('HTTP/1.1 404 Not Found');
    writeAPI(["api" => "containerAdd", "data" => $data]);
}