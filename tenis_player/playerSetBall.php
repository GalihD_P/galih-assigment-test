<?php
include("function.php");
$header = getallheaders();
$data = array_merge($_GET,$_POST);
if(isset($header['Authorization'])){
    $auth = $header['Authorization'];
    if (avail($auth) && substr($auth, 0, 7) === 'Bearer ') {
        $con = connectDB();
        $auth = substr($auth, 7);
        $player_id = checkToken($con, $auth);
            if ($player_id) {
                $sql = "SELECT c.ball_add,c.container_id,c.container_name,c.capacity FROM container c join player_to_container ptc on ptc.container_id = c.container_id WHERE ptc.player_id='$player_id';";

                $sqlCek = $con->query($sql);
                if($sqlCek){
                    $sqlCek = JsonParser($sqlCek);
                    $z = 1;
                    for ($i=0; $i < $z; $i++) { 
                       $n = rand(0,count($sqlCek)-1);
                        if((int)$sqlCek[$n]['ball_add'] < (int)$sqlCek[$n]['capacity'] ){
                            $sqlCek[$n]['ball_add'] = (int)$sqlCek[$n]['ball_add']+1;
                            $z++;
                        }
                    }

                    foreach ($sqlCek as $key) {
                        $capacity = $key['capacity'];
                        $ball_add = $key['ball_add'];
                        $container_id = $key['container_id'];
                        if($key['capacity'] == $key['ball_add']){
                            $sql = "UPDATE container SET container_status=1,capacity='$capacity',ball_add='$ball_add' WHERE container_id = '$container_id'";
                        }else{
                            $sql = "UPDATE container SET capacity='$capacity',ball_add='$ball_add' WHERE container_id = '$container_id'";
                        }
                        $sqlUpdate = $con->query($sql);
                    }

                    header('HTTP/1.1 200 OK');
                    $result = array(
                        "response" => "Success",
                        "message" => "Player Telah Diset"
                    );

                }else{
                    header('HTTP/1.1 500 Internal Server Error');
                    $result = array(
                        "response" => "Error",
                        "data" => array(
                            "error" => base64_encode($sqlCek)
                        )
                    );
                }
            } else {
                header('HTTP/1.1 401 Unauthorized');
                $result = array(
                    "response" => "Error",
                    "message" => "Unauthorized"
                );
            }
    }else{
        header('HTTP/1.1 401 Unauthorized');
        $result = array(
            "response" => "Error",
            "message" => "Unauthorized"
        );
    }    

    writeAPI(["api" => "playerSetBall", "auth" => $auth, "player_id" => isset($player_id) ? $player_id : null, "data" => $data]);
    if (isset($result))
        die(json_encode($result));
}else{
    header('HTTP/1.1 404 Not Found');
    writeAPI(["api" => "playerSetBall", "data" => $data]);
}