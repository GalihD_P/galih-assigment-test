<?php

date_default_timezone_set('asia/jakarta');

function connectDB(){
    $servername = "";
    $db_name = "tenis_player";
    $db_username = "root";
    $db_pass = "";

    $conn = new mysqli($servername, $db_username, $db_pass, $db_name);

    if($conn->connect_error) {
        file_put_contents('./connection.txt', microtime(true) . " | FAILED: " . $conn->connect_error . "\n", FILE_APPEND | LOCK_EX);
        die("Connection failed: " . $conn->connect_error . "\n");
    }
    return $conn;
}

function closeDB($conn){
    $conn->close();
}

function generateToken($con, $player_id) {
    $token = $con->query("SELECT token from tokens WHERE player_id='$player_id'");
    if ($token->num_rows > 0) {
        $token = JsonParser($token)[0];
        return $token["token"];
    } else {
        $length = 26;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, $charactersLength - 1)];
        }
        if($con->query("INSERT INTO tokens(token, player_id) VALUES ('$token', '$player_id');")) {
            return $token;
        } else {
            return null;
        }

    }
}

function JsonParser($sqlResult){
    $sqlRows = array();
    while ($row = $sqlResult->fetch_assoc()){
        $sqlRows[] = $row;
    }
    return $sqlRows;
}

function avail($var){
    return isset($var)&& trim($var) !== '' && strtolower(trim($var) !== 'null');
}

function checkToken($con, $token){
    $auth = $con->query("SELECT player_id FROM tokens WHERE token='$token';");
    if ($auth && $auth->num_rows > 0) {
        $token = JsonParser($auth)[0];
        return $token["player_id"];
    }
    return null;
}

function getIP() {
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
        if (array_key_exists($key, $_SERVER) === true){
            foreach (explode(',', $_SERVER[$key]) as $ip){
                $ip = trim($ip); // just to be safe
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                    return $ip;
                }else{
                    return $ip;
                }
            }
        }
    }
}

function writeAPI($data = []) {
    $data["date"] = date('Y-m-d H:i:s');
    $data["ip"] = getIP();
    file_put_contents('api.txt', json_encode($data) . PHP_EOL , FILE_APPEND | LOCK_EX);
}