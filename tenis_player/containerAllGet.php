<?php
include("function.php");
$header = getallheaders();
$data = array_merge($_GET,$_POST);
if(isset($header['Authorization'])){
    $auth = $header['Authorization'];
    if (avail($auth) && substr($auth, 0, 7) === 'Bearer ') {
        $con = connectDB();
        $auth = substr($auth, 7);
        $player_id = checkToken($con, $auth);
        if ($player_id) {
            $sql = "SELECT c.container_name,c.capacity FROM container c join player_to_container ptc on c.container_id = ptc.container_id join player p on p.player_id = ptc.player_id WHERE p.player_id='$player_id';";
            $sqlCek = $con->query($sql);
            if($sqlCek){
                $sqlCek = JsonParser($sqlCek);
                header('HTTP/1.1 200 OK');
                $result = array(
                    "response" => "Success",
                    "data" => $sqlCek
                );

            }else{
                header('HTTP/1.1 500 Internal Server Error');
                $result = array(
                    "response" => "Error",
                    "data" => array(
                        "error" => base64_encode($sqlCek)
                    )
                );
            }
        } else {
            header('HTTP/1.1 401 Unauthorized');
            $result = array(
                "response" => "Error",
                "message" => "Unauthorized"
            );
        }
    }else{
        header('HTTP/1.1 401 Unauthorized');
        $result = array(
            "response" => "Error",
            "message" => "Unauthorized"
        );
    }
    writeAPI(["api" => "containerAllGet", "auth" => $auth, "player_id" => isset($player_id) ? $player_id : null, "data" => $data]);
    if (isset($result))
        die(json_encode($result));
}else{
    header('HTTP/1.1 404 Not Found');
    writeAPI(["api" => "containerAllGet", "data" => $data]);
}